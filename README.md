# DK60 / DK60TP
Mechanical keyboard based on HHKB layout with or without Trackpoint.

## DK60
Just another fun(?) keyboard with HHKB layout.

Can accept normal caps lock or stepped. No backlit or RGB leds, just 2 leds on Caps lock and Esc.

All caps are in standard size to increase choice of keysets!

Accept MX, ALPS and clones switches.

Designed to fit in some 60% Poker cases.

![Layout](./Previews/DK60.png "Layout")

![PCB](./Previews/DK60-PCB.png "PCB")

Bottom row:
* 1U - 1.5U - 6.25U - 1.5U - 1U
* 1U - 1.5U - 6U - 1.5U - 1U
* 1U - 1.25U - 7U - 1.25U - 1U

## DK60TP
:warning: Still in development :warning:

DK60TP is just a big update of DK60 with support for IBM Trackpoint, backlit and RGB underglow.

My goal is to recreate a Tex Yoga like but with many custom layouts.

Electronic is divided in 3 parts:
* Keyboard PCB (compatible with some 60% Poker cases)
* Mouse buttons PCB
* IBM Trackpoint

![Layout](./Previews/DK60TP.png "Layout")

![PCB](./Previews/DK60TP-PCB.png "PCB")

![PCB](./Previews/DK60TP-PCB-FRONT.png "PCB Front")

## Firmware
DK60 is officially supported by QMK: https://github.com/qmk/qmk_firmware/tree/master/keyboards/dk60

DK60TP still in development and does not have actually associate firmware.

## Used softwares
I've only used Open Source software to make all parts of this project.

List of used softwares:
* OS: Debian
* Kicad
* QCad
* Freecad
* ...

## Third party
Libraries and Footprint are provided by Hasu, /u/techieee and mohitg11.

## External links

* PCB guide: https://github.com/ruiqimao/keyboard-pcb-guide
* Hasu's lib: https://github.com/tmk/kicad_lib_tmk
* Hasu's footprint https://github.com/tmk/keyboard_parts.pretty
* /u/techieee's footprint: https://github.com/egladman/keebs.pretty
* mohitg11's footprint and Libraries: https://github.com/mohitg11/TS65AVR
* Plate generator: http://builder.swillkb.com/
